FROM node
MAINTAINER Hector Aguirre
WORKDIR /app
COPY . .
RUN npm install
ENV MONGO_URL=mongodb://localhost:27017/todos
EXPOSE 3000
CMD MONGO_URL=${MONGO_URL} node bin/www