var express = require('express');
var router = express.Router();
const controller = require('../controllers/todos');


router.get('/', controller.list);
router.post('/save', controller.save);
router.post('/update', controller.update);
router.post('/delete', controller.delete);

module.exports = router;
