const { Todo } = require('../models/todo');

exports.list = async (req, res) => {
    try {
        const todos = await Todo.find();
        return res.json({
            error: false,
            message: 'succesfully found todos',
            object: todos
        });
    } catch (error) {
        return res.json({
            error: true,
            message: 'failed to find todos',
            object: e
        });
    }
}

exports.save = async (req, res) => {
    const name = req.body.name;

    try {
        const todo = new Todo({
            name,
            finished: false
        });
        await todo.save();
        return res.json({
            error: false,
            message: 'succesfully created new todo',
            object: todo
        });
    } catch (e) {
        return res.json({
            error: true,
            message: 'failed to create new todo',
            object: e
        });
    }
}

exports.update = async (req, res) => {
    const {
        id,
        name,
        finished
    } = req.body;

    try {
        const todo = await Todo.findById(id);
        if(todo) {
            if(name !== undefined) {
                todo.name = name;
            }
            if(finished !== undefined) {
                todo.finished = finished;
            }
            await todo.save();

            return res.json({
                error: false,
                message: 'succesfully updated new todo',
                object: todo
            });
        } else {
            return res.json({
                error: true,
                message: `couldn't find a todo with given id`,
                object: null
            });
        }
    } catch (e) {
        return res.json({
            error: true,
            message: 'failed to update new todo',
            object: e
        });
    }
}

exports.delete = async (req, res) => {
    const id = req.body.id;

    try {
        const todo = await Todo.findByIdAndDelete(id);
        if(todo) {
            return res.json({
                error: false,
                message: 'succesfully deleted new todo',
                object: todo
            });
        } else {
            return res.json({
                error: true,
                message: `couldn't find a todo with given id`,
                object: null
            });
        }
    } catch (e) {
        return res.json({
            error: true,
            message: 'failed to delete new todo',
            object: e
        });
    }
}